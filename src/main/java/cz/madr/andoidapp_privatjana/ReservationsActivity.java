package cz.madr.andoidapp_privatjana;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class ReservationsActivity extends AppCompatActivity {
 private Button addButton;
    ListView listView;
    ArrayList<Bookings> bookings = new ArrayList<Bookings>();
    TextView heading;
    private DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservations);
        heading= findViewById(R.id.headingReservations);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("bookings");
        mDatabase.orderByChild("datumOd");

        listView= findViewById(R.id.listNotes);
        listView.setFocusable(false);
        final BookingsListAdapter adapter = new BookingsListAdapter(this.getApplicationContext(), R.layout.adapter_view_layout, bookings);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bookings booking = (Bookings) parent.getItemAtPosition(position);
                Intent intent = new Intent(view.getContext(), cz.madr.andoidapp_privatjana.ReservationDetailActivity.class);
                intent.putExtra("Bookings",booking);
                startActivity(intent);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int pos, long id) {
                Bookings booking = (Bookings) parent.getItemAtPosition(pos);
                Intent intent = new Intent(view.getContext(), cz.madr.andoidapp_privatjana.ReservationEditActivity.class);
                intent.putExtra("Bookings",booking);
                startActivity(intent);
                return true;
            }
        });
        mDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Bookings value =  dataSnapshot.getValue(Bookings.class);
                bookings.add(value);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
    public void addHandler(View view)
    {
        Intent intent = new Intent(this, ReservationActivity.class);
        startActivity(intent);
    }

}
