package cz.madr.andoidapp_privatjana;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class ReservationEditActivity extends AppCompatActivity {
    private TextView headingedit;
    private EditText od;
    private EditText date2;
    private EditText jmeno;
    private EditText tel;
    private EditText email;
    private Spinner druh;
    private EditText cena;
    private Spinner mena;
    private CheckBox checkBox1;
    private CheckBox checkBox2;
    private CheckBox checkBox3;
    private CheckBox checkBox4;
    private CheckBox checkBox5;
    private DatabaseReference mDatabase;
    Bookings booking;
    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(this, cz.madr.andoidapp_privatjana.ReservationsActivity.class);
        startActivity(intent);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation_edit);
        headingedit=findViewById(R.id.headingEdit);
        od = findViewById(R.id.datumEdit);
        date2 = findViewById(R.id.date2Edit);
        jmeno = findViewById(R.id.jmenoEdit);
        email=findViewById(R.id.emailEdit);
        tel= findViewById(R.id.tel2Edit);
        druh = findViewById(R.id.druhEdit);
        cena = findViewById(R.id.cenaEdit);
        mena = findViewById(R.id.menaEdit);
        checkBox1 = findViewById(R.id.checkBox1Edit);
        checkBox2 = findViewById(R.id.checkBox2Edit);
        checkBox3 = findViewById(R.id.checkBox3Edit);
        checkBox4 = findViewById(R.id.checkBox4Edit);
        checkBox5 = findViewById(R.id.checkBox5Edit);
        booking = new Bookings();

        Bookings tempbooking = (Bookings) getIntent().getSerializableExtra("Bookings");
        headingedit.setText("Rezervace \n"+tempbooking.getJmeno());
        mDatabase = FirebaseDatabase.getInstance().getReference().child("bookings").child(tempbooking.getId());
        od.setText(tempbooking.getDatumOd());
        date2.setText(tempbooking.getDatumDo());
        jmeno.setText(tempbooking.getJmeno());
        email.setText(tempbooking.getEmail());
        tel.setText(tempbooking.getTel());


        List<String> currency = new ArrayList<String>();
        currency.add("EUR");
        currency.add("CZK");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, currency);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mena.setAdapter(dataAdapter);
        switch (tempbooking.getMena()){
            case "EUR":
                druh.setSelection(0);
                break;
            case "CZK":
                druh.setSelection(1);
                break;
        }

        List<String> type = new ArrayList<String>();
        type.add("NON");
        type.add("Booking");
        type.add("UPHILL");
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, type);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        druh.setAdapter(dataAdapter2);
        switch (tempbooking.getDruh()){
            case "NON":
                druh.setSelection(0);
                break;
            case "Booking":
                druh.setSelection(1);
                break;
            case "UPHILL":
                druh.setSelection(2);
                break;
        }

        cena.setText(new Integer(tempbooking.getCena()).toString());
        if(tempbooking.isPokoj1())
        {
            checkBox1.setChecked(true);
        }
        if(tempbooking.isPokoj2())
        {
            checkBox2.setChecked(true);
        }
        if(tempbooking.isPokoj3())
        {
            checkBox3.setChecked(true);
        }
        if(tempbooking.isPokoj4())
        {
            checkBox4.setChecked(true);
        }
        if(tempbooking.isPokoj5())
        {
            checkBox5.setChecked(true);
        }
    booking.setId(tempbooking.getId());


    }
    public void deleteReservation(View view)
    {
        mDatabase.removeValue();
        Intent intent = new Intent(this, ReservationsActivity.class);
        startActivity(intent);

    }
    public void  updateReservation(View view){
        booking.setDatumOd(od.getText().toString().trim());
        booking.setDatumDo(date2.getText().toString().trim());
        booking.setDruh(druh.getSelectedItem().toString());
        booking.setJmeno(jmeno.getText().toString().trim());
        booking.setEmail(email.getText().toString().trim());
        booking.setTel(tel.getText().toString().trim());
        booking.setCena(Integer.parseInt(cena.getText().toString()));
        booking.setMena(mena.getSelectedItem().toString().trim());
        if (checkBox1.isChecked()) {
            booking.setPokoj1(true);
        } else {
            booking.setPokoj1(false);
        }
        if (checkBox2.isChecked()) {
            booking.setPokoj2(true);
        } else {
            booking.setPokoj2(false);
        }
        if (checkBox3.isChecked()) {
            booking.setPokoj3(true);
        }else
        {
            booking.setPokoj3(false);
        }
        if (checkBox4.isChecked()) {
            booking.setPokoj4(true);
        }else
        {
            booking.setPokoj4(false);
        }
        if (checkBox5.isChecked()) {
            booking.setPokoj5(true);
        }else
        {
            booking.setPokoj5(false);
        }
        mDatabase.setValue(booking);

        Intent intent = new Intent(this, ReservationsActivity.class);
        startActivity(intent);

        }
}
