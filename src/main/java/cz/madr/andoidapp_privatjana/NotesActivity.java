package cz.madr.andoidapp_privatjana;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;

public class NotesActivity extends AppCompatActivity {
    Bookings booking;
    Note note;
    EditText datum;
    EditText text;
    private DatabaseReference mDatabase;
    private DatePickerDialog.OnDateSetListener onDateSetListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);
        datum= findViewById(R.id.datumEdit);

        datum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                String prevDate = datum.getText().toString().trim();
                String [] chars = prevDate.split("\\.");

                cal.set(Calendar.YEAR,Integer.parseInt(chars[2]));
                cal.set(Calendar.MONTH,Integer.parseInt(chars[1]));
                cal.set(Calendar.DAY_OF_MONTH,Integer.parseInt(chars[0]));
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(NotesActivity.this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth,onDateSetListener,year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        onDateSetListener= new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month+1;
                String date = dayOfMonth+"."+month+"."+year;
                datum.setText(date);
            }
        };


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        text = findViewById(R.id.editNoteText);
        booking = (Bookings) getIntent().getSerializableExtra("Bookings");
        datum.setText(booking.getDatumOd());
        note=new Note();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("notes");

    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(this, cz.madr.andoidapp_privatjana.ReservationDetailActivity.class);
        intent.putExtra("Bookings",booking);
        startActivity(intent);
        return true;
    }
    public void saveNote(View view)
    {
        note.setDatum(datum.getText().toString().trim());
        note.setText(text.getText().toString().trim());
        note.setBookingID(booking.id);
        String id = mDatabase.push().getKey();
        note.setId(id);
        note.setImagePath(null);
        mDatabase.child(id).setValue(note);
        Intent intent = new Intent(view.getContext(), cz.madr.andoidapp_privatjana.ReservationDetailActivity.class);
        intent.putExtra("Bookings",booking);
        startActivity(intent);

    }

}
