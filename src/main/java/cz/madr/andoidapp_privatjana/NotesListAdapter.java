package cz.madr.andoidapp_privatjana;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class NotesListAdapter extends ArrayAdapter<Note> {
    private static final String TAG = "NotesListAdapter";

    private Context mContext;
    private int mResource;

    public NotesListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Note> objects) {
        super(context, resource, objects);
        this.mContext = context;
        mResource=resource;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Note note = new Note();
        note = getItem(position);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);
        TextView datum = (TextView) convertView.findViewById(R.id.textView3);
        TextView text = (TextView) convertView.findViewById(R.id.textView1);


        datum.setText(note.getDatum());
        text.setText(note.getText());
        return convertView;

    }
}
