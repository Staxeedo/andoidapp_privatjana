package cz.madr.andoidapp_privatjana;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ReservationDetailActivity extends AppCompatActivity {
    private DatabaseReference mDatabase;
    TextView heading;
    TextView detailINFO;
    TextView detailPokoje;
    TextView detailOD;
    TextView detailDO;
    TextView detailCena;
    Bookings booking;
    ListView listView;
    ArrayList<Note> notes = new ArrayList<Note>();

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(this, cz.madr.andoidapp_privatjana.ReservationsActivity.class);
        startActivity(intent);
        return true;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        booking = (Bookings) getIntent().getSerializableExtra("Bookings");
        mDatabase = FirebaseDatabase.getInstance().getReference().child("notes");
        mDatabase.equalTo(booking.getId(),"bookingID");
        heading = findViewById(R.id.headingDetail2);
        heading.setText(booking.getJmeno());
        switch (booking.getDruh()){
            case "Booking":
                    heading.setTextColor(Color.parseColor("#226666"));
                    break;
            case "NON":
                heading.setTextColor(Color.parseColor("#FF8521"));
                break;
            case "UPHILL":
                heading.setTextColor(Color.parseColor("#FFA645"));
                break;
        }
        detailINFO = findViewById(R.id.detailINFO);
        detailINFO.setText(booking.getEmail()+'\n'+booking.getTel());
        detailPokoje=findViewById(R.id.detailPokoje);
        detailPokoje.setText(booking.pokojeToString());
        detailOD=findViewById(R.id.detailOD);
        detailOD.setText(booking.getDatumOd());
        detailDO=findViewById(R.id.detailDO);
        detailDO.setText(booking.getDatumDo());
        detailCena=findViewById(R.id.detailCena);
        String cena="";
        switch (booking.getMena()){
            case "EUR":
                cena = booking.getCena()+" EUR \n"+ (booking.getCena()*4)+" PLN \n"+ (booking.getCena() * 27)+" CZK";
                break;
            case "CZK":
                cena = booking.getCena()/27+" EUR\n"+(booking.getCena()/6)+" PLN\n"+booking.getCena()+" CZK";
                break;
        }

        detailCena.setText(cena);



        listView= findViewById(R.id.listNotes);
        listView.setFocusable(false);
        final NotesListAdapter adapter = new NotesListAdapter(this.getApplicationContext(), R.layout.adapter_note_view_layout, notes);
        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Note note = (Note) parent.getItemAtPosition(position);
                Intent intent = new Intent(view.getContext(), cz.madr.andoidapp_privatjana.NoteDetailActivity.class);
                intent.putExtra("Note",note);
                intent.putExtra("Bookings",booking);

                startActivity(intent);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int pos, long id) {
                Note note = (Note) parent.getItemAtPosition(pos);
                DatabaseReference reff = FirebaseDatabase.getInstance().getReference().child("notes").child(note.getId());
                reff.removeValue();
                Intent intent = new Intent(parent.getContext(), cz.madr.andoidapp_privatjana.ReservationDetailActivity.class);
                intent.putExtra("Bookings",booking);
                startActivity(intent);
                return true;
            }
        });

        mDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Note value =  dataSnapshot.getValue(Note.class);
                if(value.getBookingID().equals(booking.getId()))
                    notes.add(value);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
    public void createNote(View view)
    {
        Intent intent = new Intent(view.getContext(), cz.madr.andoidapp_privatjana.NotesActivity.class);
        intent.putExtra("Bookings",booking);
        startActivity(intent);

    }
}
