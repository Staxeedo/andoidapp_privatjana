package cz.madr.andoidapp_privatjana;
import java.io.Serializable;
import com.google.firebase.database.PropertyName;

import java.util.Date;

public class Bookings implements Serializable  {


    String id;
    String datumOd;
    String datumDo;
    String druh;
    String jmeno;
    String email;
    String tel;
    int cena;
    String mena;
    boolean pokoj1;
    boolean pokoj2;
    boolean pokoj3;
    boolean pokoj4;
    boolean pokoj5;

    public Bookings() {

    }
    public Bookings(String id,String datumOd, String datumDo, String druh, String jmeno, int cena, String mena,String email,String tel,boolean pokoj1, boolean pokoj2, boolean pokoj3, boolean pokoj4, boolean pokoj5) {
        this.datumOd = datumOd;
        this.datumDo = datumDo;
        this.druh = druh;
        this.jmeno = jmeno;
        this.email = email;
        this.tel=tel;
        this.cena = cena;
        this.mena = mena;
        this.pokoj1 = pokoj1;
        this.pokoj2 = pokoj2;
        this.pokoj3 = pokoj3;
        this.pokoj4 = pokoj4;
        this.pokoj5 = pokoj5;
    }
    public String getId() {
        return id;
    }

    public  void setId(String id) {
        this.id = id;
    }
    public String getEmail() {
        return email;
    }

    public String getTel() {
        return tel;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getDatumOd() {
        return datumOd;
    }

    public void setDatumOd(String datumOd) {

        this.datumOd = datumOd;
    }

    public String getDatumDo() {
        return datumDo;
    }

    public void setDatumDo(String datumDo) {
        this.datumDo = datumDo;
    }

    public String getDruh() {
        return druh;
    }

    public void setDruh(String druh) {
        this.druh = druh;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public String getMena() {
        return mena;
    }

    public void setMena(String mena) {
        this.mena = mena;
    }

    public boolean isPokoj1() {
        return pokoj1;
    }

    public void setPokoj1(boolean pokoj1) {
        this.pokoj1 = pokoj1;
    }

    public boolean isPokoj2() {
        return pokoj2;
    }

    public void setPokoj2(boolean pokoj2) {
        this.pokoj2 = pokoj2;
    }

    public boolean isPokoj3() {
        return pokoj3;
    }

    public void setPokoj3(boolean pokoj3) {
        this.pokoj3 = pokoj3;
    }

    public boolean isPokoj4() {
        return pokoj4;
    }

    public void setPokoj4(boolean pokoj4) {
        this.pokoj4 = pokoj4;
    }

    public boolean isPokoj5() {
        return pokoj5;
    }

    public void setPokoj5(boolean pokoj5) {
        this.pokoj5 = pokoj5;
    }


    public String pokojeToString(){
        String text = "";
        if(pokoj1==true){
            text=text+" 1 ";
        }
        if(pokoj2==true){
            text=text+" 2 ";
        }
        if(pokoj3==true){
            text=text+" 3 ";
        }
        if(pokoj4==true){
            text=text+" 4 ";
        }
        if(pokoj5==true){
            text=text+" 5 ";
        }
        return text;
    }
    @Override
    public String toString() {
        return datumOd + " - " + datumDo + " " + jmeno;
    }
}
