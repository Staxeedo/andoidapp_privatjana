package cz.madr.andoidapp_privatjana;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ReservationActivity extends AppCompatActivity {
    private EditText od;
    private EditText date2;
    private EditText jmeno;
    private EditText tel;
    private EditText email;
    private Spinner druh;
    private EditText cena;
    private Spinner mena;
    private CheckBox checkBox1;
    private CheckBox checkBox2;
    private CheckBox checkBox3;
    private CheckBox checkBox4;
    private CheckBox checkBox5;
    private DatePickerDialog.OnDateSetListener onDateSetListener;
    private DatePickerDialog.OnDateSetListener date2SetListener;
    Button button;
    private DatabaseReference mDatabase;
    Bookings booking;

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(this, cz.madr.andoidapp_privatjana.ReservationsActivity.class);
        startActivity(intent);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        od = findViewById(R.id.datumEdit);
        od.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(ReservationActivity.this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth,onDateSetListener,year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        onDateSetListener= new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month+1;
                String date = dayOfMonth+"."+month+"."+year;
                od.setText(date);
            }
        };




        date2 = findViewById(R.id.date2Edit);
        date2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(ReservationActivity.this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth,date2SetListener,year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        date2SetListener= new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month+1;
                String date = dayOfMonth+"."+month+"."+year;
                date2.setText(date);
            }
        };
        jmeno = findViewById(R.id.jmenoEdit);
        email=findViewById(R.id.emailEdit);
        tel= findViewById(R.id.tel2Edit);
        druh = findViewById(R.id.druhEdit);
        cena = findViewById(R.id.cenaEdit);
        mena = findViewById(R.id.menaEdit);
        checkBox1 = findViewById(R.id.checkBox1Edit);
        checkBox2 = findViewById(R.id.checkBox2Edit);
        checkBox3 = findViewById(R.id.checkBox3Edit);
        checkBox4 = findViewById(R.id.checkBox4Edit);
        checkBox5 = findViewById(R.id.checkBox5Edit);
        button = findViewById(R.id.buttonEdit);



        List<String> currency = new ArrayList<String>();
        currency.add("EUR");
        currency.add("CZK");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, currency);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mena.setAdapter(dataAdapter);

        List<String> type = new ArrayList<String>();
        type.add("NON");
        type.add("Booking");
        type.add("UPHILL");
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, type);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        druh.setAdapter(dataAdapter2);

        booking = new Bookings();
         mDatabase = FirebaseDatabase.getInstance().getReference().child("bookings");

    }

    public void addReservation(View view) {
        booking.setDatumOd(od.getText().toString().trim());
        booking.setDatumDo(date2.getText().toString().trim());
        booking.setDruh(druh.getSelectedItem().toString());
        booking.setJmeno(jmeno.getText().toString().trim());
        booking.setEmail(email.getText().toString().trim());
        booking.setTel(tel.getText().toString().trim());
        booking.setCena(Integer.parseInt(cena.getText().toString()));
        booking.setMena(mena.getSelectedItem().toString().trim());
        if (checkBox1.isChecked()) {
            booking.setPokoj1(true);
        } else {
            booking.setPokoj1(false);
        }
        if (checkBox2.isChecked()) {
            booking.setPokoj2(true);
        } else {
            booking.setPokoj2(false);
        }
        if (checkBox3.isChecked()) {
            booking.setPokoj3(true);
        }else
        {
            booking.setPokoj3(false);
        }
        if (checkBox4.isChecked()) {
            booking.setPokoj4(true);
        }else
        {
            booking.setPokoj4(false);
        }
        if (checkBox5.isChecked()) {
            booking.setPokoj5(true);
        }else
        {
            booking.setPokoj5(false);
        }
        String id = mDatabase.push().getKey();
        booking.setId(id);
        mDatabase.child(id).setValue(booking);
        Intent intent = new Intent(this, ReservationsActivity.class);
        startActivity(intent);

    }
}
