package cz.madr.andoidapp_privatjana;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class BookingsListAdapter extends ArrayAdapter<Bookings> {
    private static final String TAG = "PersonListAdapter";

    private Context mContext;
    private int mResource;

    public BookingsListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Bookings> objects) {
        super(context, resource, objects);
        this.mContext = context;
        mResource=resource;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Bookings booking = new Bookings();
       booking = getItem(position);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);
        TextView jmeno = (TextView) convertView.findViewById(R.id.textView1);
        TextView od = (TextView) convertView.findViewById(R.id.textView2);
        TextView datum2 = (TextView) convertView.findViewById(R.id.textView3);

        if(booking.getDruh().equals("Booking"))
        {
            convertView.setBackgroundColor(Color.parseColor("#226666"));
            jmeno.setTextColor(Color.parseColor("#FFFFFF"));
            od.setTextColor(Color.parseColor("#FFFFFF"));
            datum2.setTextColor(Color.parseColor("#FFFFFF"));

        }else if( booking.getDruh().equals("UPHILL"))
        {
            convertView.setBackgroundColor(Color.parseColor("#FF8521"));
            jmeno.setTextColor(Color.parseColor("#FFFFFF"));
            od.setTextColor(Color.parseColor("#FFFFFF"));
            datum2.setTextColor(Color.parseColor("#FFFFFF"));
        }
        else{
            convertView.setBackgroundColor(Color.parseColor("#FFA645"));
            jmeno.setTextColor(Color.parseColor("#FFFFFF"));
            od.setTextColor(Color.parseColor("#FFFFFF"));
            datum2.setTextColor(Color.parseColor("#FFFFFF"));

        }


        jmeno.setText(booking.getJmeno());
        od.setText(booking.getDatumOd());
        datum2.setText(booking.getDatumDo());
        return convertView;

    }
}
