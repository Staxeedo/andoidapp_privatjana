package cz.madr.andoidapp_privatjana;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.io.IOException;

public class NoteDetailActivity extends AppCompatActivity implements SensorEventListener {
    TextView text;
    TextView datum;
    ImageView imageView;
    Note note;
    Bookings booking;
    ImageButton button;
    boolean isAvailable = true;
    boolean notFirstTime = false;
    private SensorManager sensorManager;
    private Sensor accel;
    private float x, y, z;
    private float prevX, prevY, prevZ;
    private float xDiff, yDiff, zDiff;
    private float shakeThr = 5f;
    Context context = this;
    String currentImagePath=null;
    private static final int IMAGE_REQ = 1;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        note = (Note) getIntent().getSerializableExtra("Note");
        booking = (Bookings) getIntent().getSerializableExtra("Bookings");
        imageView = findViewById(R.id.foto);
        button=findViewById(R.id.imageButton);
        if(note.getImagePath()==null)
        {
            imageView.setVisibility(View.INVISIBLE);

        }
        else
        {
            button.setVisibility(View.INVISIBLE);
            Bitmap bitmap = BitmapFactory.decodeFile(note.getImagePath());
            imageView.setImageBitmap(bitmap);
        }

        text = findViewById(R.id.textViewTextPozn);
        datum = findViewById(R.id.textViewDatum);
        text.setText(note.getText());
        datum.setText(note.getDatum());

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        } else {
            isAvailable = false;
        }

        mDatabase = FirebaseDatabase.getInstance().getReference().child("notes").child(note.getId());

    }
    public void takeImage(View view)
    {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(cameraIntent.resolveActivity(getPackageManager())!=null){
            File imageFile = null;
            try {
                imageFile = getImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(imageFile != null)
            {
                Uri imageUri = FileProvider.getUriForFile(this,"com.example.android.fileprovider",imageFile);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,imageUri);
                startActivityForResult(cameraIntent,IMAGE_REQ);
                note.setImagePath(currentImagePath);
                mDatabase.setValue(note);

            }

        }

    }
    private File getImageFile() throws IOException
    {
        String imageName = note.getId();
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File imageFile = File.createTempFile(imageName,".jpg",storageDir);
        currentImagePath = imageFile.getAbsolutePath();
        return imageFile;
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(this, cz.madr.andoidapp_privatjana.ReservationDetailActivity.class);
        intent.putExtra("Bookings", booking);
        startActivity(intent);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isAvailable) {
            sensorManager.registerListener(this, accel, SensorManager.SENSOR_DELAY_NORMAL);

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isAvailable) {
            sensorManager.unregisterListener(this);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        x = event.values[0];
        y = event.values[1];
        z = event.values[2];
        if (notFirstTime) {
            float xDiff = Math.abs(prevX - x);
            float yDiff = Math.abs(prevY - y);
            float zDiff = Math.abs(prevZ - z);

            if ((xDiff > shakeThr && yDiff > shakeThr)
                    || (xDiff > shakeThr && zDiff > shakeThr)
                    || (yDiff > shakeThr && zDiff > shakeThr)) {
                text.setText("SHAKED");
                DatabaseReference reff = FirebaseDatabase.getInstance().getReference().child("notes").child(note.getId());
                reff.removeValue();
                Intent intent = new Intent(context, cz.madr.andoidapp_privatjana.ReservationDetailActivity.class);
                intent.putExtra("Bookings",booking);
                startActivity(intent);

            }
        }
        prevX = x;
        prevY = y;
        prevZ = z;
        notFirstTime = true;

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
